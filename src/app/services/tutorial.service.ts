import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Tutorial } from '../models/tutorial.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TutorialService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Tutorial[]> {
    console.log(environment.URL_BACK);
    return this.http.get<Tutorial[]>(environment.URL_BACK);
  }

  get(id: any): Observable<Tutorial> {
    return this.http.get(`${environment.URL_BACK}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(environment.URL_BACK, data);
  }

  update(id: any, data: any): Observable<any> {
    return this.http.put(`${environment.URL_BACK}/${id}`, data);
  }

  delete(id: any): Observable<any> {
    return this.http.delete(`${environment.URL_BACK}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(environment.URL_BACK);
  }

  findByTitle(title: any): Observable<Tutorial[]> {
    return this.http.get<Tutorial[]>(`${environment.URL_BACK}?title=${title}`);
  }
}
