export const environment = {
  production: true,
  URL_BACK: 'https://tpdocker-back.herokuapp.com/api/tutorials',
  PORT: 3000
};
