# Docker cmd

To build : docker build . -t "deploytpfront"

To run : docker run --name frontend -d deploytpfront:latest

# Heroku
Deploy at : https://tpdocker-front.herokuapp.com/tutorials

Deploy by dockerfile :

- git add .
- git commit -m "text of commit"
- heroku stack:set container
- git push heroku main
